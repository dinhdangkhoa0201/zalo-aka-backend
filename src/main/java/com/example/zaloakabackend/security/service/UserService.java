package com.example.zaloakabackend.security.service;

import com.example.zaloakabackend.entity.UserAccountEntity;
import com.example.zaloakabackend.repository.UserAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.info("[loadUserByUsername]");
        UserAccountEntity userAccountEntity = userAccountRepository.findByEmail(username);
        if (userAccountEntity == null) {
            LOGGER.error("[UsernameNotFoundException]");
            throw new UsernameNotFoundException(username);
        }
        return new UserDetail(userAccountEntity);
    }

    public UserDetails findById(Long id) {
        LOGGER.info("[findById]");
        UserDetails userDetails = null;
        try {
            Optional<UserAccountEntity> optional = userAccountRepository.findById(id);
            if (optional.isPresent()) {
                userDetails = new UserDetail(optional.get());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return userDetails;
    }
}
