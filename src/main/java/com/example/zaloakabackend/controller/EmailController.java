package com.example.zaloakabackend.controller;

import com.example.zaloakabackend.dto.EmailDTO;
import com.example.zaloakabackend.service.IEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/email")
public class EmailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private IEmailService iEmailService;

    @RequestMapping(path = "/sendEmail", method = RequestMethod.POST)
    public ResponseEntity<?> sendEmail(@RequestBody EmailDTO emailDTO) {
        LOGGER.info("[sendEmail]");
        iEmailService.sendEmail(emailDTO);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
