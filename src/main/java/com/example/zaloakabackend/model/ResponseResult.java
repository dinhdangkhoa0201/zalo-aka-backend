package com.example.zaloakabackend.model;

import com.example.zaloakabackend.enums.ResponseStatusFlag;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ResponseResult<T> implements Serializable {
    private T object;
    private ResponseStatusFlag status;
    private String message;

    public ResponseResult(ResponseStatusFlag status, T object) {
        this.status = status;
        this.object = object;
    }

    public ResponseResult(ResponseStatusFlag status) {
        this.status = status;
    }
}
