package com.example.zaloakabackend.service;

import com.example.zaloakabackend.dto.ValidationTokenDTO;
import com.example.zaloakabackend.model.ResponseResult;

public interface IValidationTokenService {
    ResponseResult<ValidationTokenDTO> createRefreshToken(Long userAccountId);
}
