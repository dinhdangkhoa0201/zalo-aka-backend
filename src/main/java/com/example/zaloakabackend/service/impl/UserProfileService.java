package com.example.zaloakabackend.service.impl;

import com.example.zaloakabackend.constants.ErrorMessageConstant;
import com.example.zaloakabackend.constants.MessageConstant;
import com.example.zaloakabackend.dto.*;
import com.example.zaloakabackend.entity.UserAccountEntity;
import com.example.zaloakabackend.entity.UserProfileEntity;
import com.example.zaloakabackend.entity.ValidationTokenEntity;
import com.example.zaloakabackend.enums.ActiveFlag;
import com.example.zaloakabackend.enums.ResponseStatusFlag;
import com.example.zaloakabackend.enums.UsableFlag;
import com.example.zaloakabackend.model.ResponseResult;
import com.example.zaloakabackend.repository.UserAccountRepository;
import com.example.zaloakabackend.repository.UserProfileRepository;
import com.example.zaloakabackend.repository.ValidationTokenRepository;
import com.example.zaloakabackend.request.VerifyTokenRequest;
import com.example.zaloakabackend.response.ErrorMessage;
import com.example.zaloakabackend.response.RegisterResponse;
import com.example.zaloakabackend.service.IEmailService;
import com.example.zaloakabackend.service.IUserProfileService;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserProfileService implements IUserProfileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileService.class);

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private ValidationTokenRepository validationTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IEmailService iEmailService;

    @Override
    @Transactional
    public ResponseResult<Object> register(RegisterDTO registerDTO) {
        LOGGER.info("[register]");
        ResponseResult<Object> result = new ResponseResult<>(ResponseStatusFlag.FAILURE);
        try {
            if (!userAccountRepository.existsByEmail(registerDTO.getEmail())) {
                LOGGER.info("- set value for User Profile");
                UserProfileEntity userProfileEntity = new UserProfileEntity();
                userProfileEntity.setFirstName(registerDTO.getFirstName());
                userProfileEntity.setLastName(registerDTO.getLastName());
                userProfileEntity.setActiveFlag(ActiveFlag.INACTIVE);
                userProfileEntity.setUsableFlag(UsableFlag.USABLE);
                userProfileEntity.setCreateBy(registerDTO.getEmail());
                userProfileEntity.setCreateDate(LocalDateTime.now());
                userProfileEntity.setUpdateBy(registerDTO.getEmail());
                userProfileEntity.setUpdateDate(LocalDateTime.now());
                userProfileEntity = userProfileRepository.save(userProfileEntity);

                LOGGER.info("- set value for User Account");
                UserAccountEntity userAccountEntity = new UserAccountEntity();
                userAccountEntity.setEmail(registerDTO.getEmail());
                userAccountEntity.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
                userAccountEntity.setActiveFlag(ActiveFlag.INACTIVE);
                userAccountEntity.setUsableFlag(UsableFlag.USABLE);
                userAccountEntity.setCreateBy(registerDTO.getEmail());
                userAccountEntity.setCreateDate(LocalDateTime.now());
                userAccountEntity.setUpdateBy(registerDTO.getEmail());
                userAccountEntity.setUpdateDate(LocalDateTime.now());
                userAccountEntity.setUserProfile(userProfileEntity);
                userAccountEntity = userAccountRepository.save(userAccountEntity);

                LOGGER.info("- set value for Validation Token");
                String token = RandomStringUtils.randomAlphabetic(10);
                ValidationTokenEntity validationTokenEntity = new ValidationTokenEntity();
                validationTokenEntity.setToken(token);
                validationTokenEntity.setExpirationDate(LocalDateTime.now().plusSeconds(MessageConstant.VALIDATION_EXPIRED_TIME));
                validationTokenEntity.setUserAccount(userAccountEntity);
                validationTokenEntity.setActiveFlag(ActiveFlag.ACTIVE);
                validationTokenEntity.setUsableFlag(UsableFlag.USABLE);
                validationTokenEntity.setCreateBy(registerDTO.getEmail());
                validationTokenEntity.setCreateDate(LocalDateTime.now());
                validationTokenEntity.setUpdateBy(registerDTO.getEmail());
                validationTokenEntity.setUpdateDate(LocalDateTime.now());
                validationTokenRepository.save(validationTokenEntity);

                LOGGER.info("- send email");
                EmailDTO emailDTO = new EmailDTO();
                List<EmailItemDTO> emailTos = new ArrayList<>();
                emailTos.add(new EmailItemDTO(registerDTO.getEmail()));
                emailDTO.setEmailTo(emailTos);
                emailDTO.setSubject("Zalo Aka - Verify Account");
                emailDTO.setContent("Zalo Aka - Verify Account: " + token);
                iEmailService.sendEmail(emailDTO);

                RegisterResponse registerResponse = new RegisterResponse();
                UserAccountDTO userAccountDTO = new UserAccountDTO();
                BeanUtils.copyProperties(userAccountEntity, userAccountDTO);
                registerResponse.setUserAccountDTO(userAccountDTO);
                registerResponse.setToken(token);
                result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, registerResponse);
            } else {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setStatusCode(HttpStatus.BAD_REQUEST);
                errorMessage.setTimestamp(LocalDateTime.now());
                errorMessage.setMessage(ErrorMessageConstant.EMAIL_EXISTED);
                result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, errorMessage);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    @Override
    public ResponseResult<Object> verify(VerifyTokenRequest request) {
        LOGGER.info("[verify]");
        ResponseResult<Object> result = new ResponseResult<>(ResponseStatusFlag.FAILURE);
        try {
            Optional<ValidationTokenEntity> optional = validationTokenRepository.findByToken(request.getToken());
            if (optional.isPresent()) {
                ValidationTokenEntity validationTokenEntity = optional.get();
                UserAccountEntity userAccountEntity = validationTokenEntity.getUserAccount();
                if (validationTokenEntity.getExpirationDate().compareTo(LocalDateTime.now()) < 0) {
                    LOGGER.info("- delete Validation Token");
                    validationTokenRepository.deleteById(validationTokenEntity.getId());

                    LOGGER.info("- create new Validation Token");
                    String token = RandomStringUtils.randomAlphabetic(10);
                    ValidationTokenEntity validationTokenNew = new ValidationTokenEntity();
                    validationTokenNew.setToken(token);
                    validationTokenEntity.setExpirationDate(LocalDateTime.now().plusSeconds(MessageConstant.VALIDATION_EXPIRED_TIME));
                    validationTokenNew.setUserAccount(userAccountEntity);
                    validationTokenNew.setActiveFlag(ActiveFlag.ACTIVE);
                    validationTokenNew.setUsableFlag(UsableFlag.USABLE);
                    validationTokenNew.setCreateBy(MessageConstant.SYSTEM);
                    validationTokenNew.setCreateDate(LocalDateTime.now());
                    validationTokenNew.setUpdateBy(MessageConstant.SYSTEM);
                    validationTokenNew.setUpdateDate(LocalDateTime.now());
                    validationTokenRepository.save(validationTokenNew);

                    LOGGER.info("- send email");
                    EmailDTO emailDTO = new EmailDTO();
                    StringBuilder sb = new StringBuilder();
                    sb.append("<p>Zalo Aka - Verify Account</p>");
                    sb.append("<strong>Code: ");
                    sb.append(token);
                    sb.append("</strong>");
                    List<EmailItemDTO> emailTos = new ArrayList<>();
                    emailTos.add(new EmailItemDTO(userAccountEntity.getEmail()));
                    emailDTO.setEmailTo(emailTos);
                    emailDTO.setEmailTo(emailTos);
                    emailDTO.setSubject("Zalo Aka - Verify Account");
                    emailDTO.setContent(sb.toString());
                    iEmailService.sendEmail(emailDTO);

                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setStatusCode(HttpStatus.BAD_REQUEST);
                    errorMessage.setTimestamp(LocalDateTime.now());
                    errorMessage.setMessage(ErrorMessageConstant.TOKEN_EXPIRED);

                    result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, errorMessage);
                } else {
                    LOGGER.info("- update User Profile");
                    UserProfileEntity userProfileEntity = userAccountEntity.getUserProfile();
                    userProfileEntity.setActiveFlag(ActiveFlag.ACTIVE);
                    userProfileEntity.setUpdateBy(userAccountEntity.getEmail());
                    userProfileEntity.setUpdateDate(LocalDateTime.now());
                    userProfileRepository.save(userProfileEntity);

                    LOGGER.info("- update User Account");
                    userAccountEntity.setActiveFlag(ActiveFlag.ACTIVE);
                    userAccountEntity.setUpdateBy(userAccountEntity.getEmail());
                    userAccountEntity.setUpdateDate(LocalDateTime.now());
                    userAccountRepository.save(userAccountEntity);

                    LOGGER.info("- delete Validation Token");
                    validationTokenRepository.deleteById(validationTokenEntity.getId());

                    UserProfileDTO userProfileDTO = new UserProfileDTO();
                    BeanUtils.copyProperties(userProfileEntity, userProfileDTO);
                    result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, userProfileDTO);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    @Override
    public ResponseResult<List<UserProfileDTO>> findAll() {
        return null;
    }
}
