package com.example.zaloakabackend.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TBL_M_ROLE")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "activeFlag", column = @Column(name = "ACTIVE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "usableFlag", column = @Column(name = "USABLE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "createBy", column = @Column(name = "CREATE_BY")),
        @AttributeOverride(name = "createDate", column = @Column(name = "CREATE_DATE")),
        @AttributeOverride(name = "updateBy", column = @Column(name = "UPDATE_BY")),
        @AttributeOverride(name = "updateDate", column = @Column(name = "UPDATE_DATE")),
})
@Data
public class RoleEntity extends AbstractEntity implements Serializable {

    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESC")
    private String desc;
}
