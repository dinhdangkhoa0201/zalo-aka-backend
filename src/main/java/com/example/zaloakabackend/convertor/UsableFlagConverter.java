package com.example.zaloakabackend.convertor;

import com.example.zaloakabackend.enums.UsableFlag;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class UsableFlagConverter implements AttributeConverter<UsableFlag, String> {
    @Override
    public String convertToDatabaseColumn(UsableFlag usableFlag) {
        if (usableFlag == null)
            return null;
        return usableFlag.getCode();
    }

    @Override
    public UsableFlag convertToEntityAttribute(String s) {
        if (s == null)
            return null;
        return UsableFlag.fromCode(s);
    }
}
