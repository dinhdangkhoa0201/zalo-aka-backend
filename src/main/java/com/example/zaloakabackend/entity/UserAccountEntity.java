package com.example.zaloakabackend.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "TBL_M_USER_ACCOUNT")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "activeFlag", column = @Column(name = "ACTIVE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "usableFlag", column = @Column(name = "USABLE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "createBy", column = @Column(name = "CREATE_BY")),
        @AttributeOverride(name = "createDate", column = @Column(name = "CREATE_DATE")),
        @AttributeOverride(name = "updateBy", column = @Column(name = "UPDATE_BY")),
        @AttributeOverride(name = "updateDate", column = @Column(name = "UPDATE_DATE")),
})
@Data
public class UserAccountEntity extends AbstractEntity implements Serializable {

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "TBL_USER_ACCOUNT_ROLE",
            joinColumns = @JoinColumn(name = "USER_ACCOUNT_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID")
    )
    private List<RoleEntity> roles;

    @OneToOne
    @JoinColumn(name = "USER_PROFILE_ID")
    private UserProfileEntity userProfile;
}
