package com.example.zaloakabackend;

import com.example.zaloakabackend.constants.MessageConstant;
import com.example.zaloakabackend.entity.UserAccountEntity;
import com.example.zaloakabackend.entity.UserProfileEntity;
import com.example.zaloakabackend.enums.ActiveFlag;
import com.example.zaloakabackend.enums.GenderFlag;
import com.example.zaloakabackend.enums.UsableFlag;
import com.example.zaloakabackend.repository.UserAccountRepository;
import com.example.zaloakabackend.repository.UserProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.time.LocalDateTime;


@SpringBootApplication
@EnableWebMvc
public class ZaloAkaBackendApplication implements CommandLineRunner {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ZaloAkaBackendApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ZaloAkaBackendApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("\n" +
                " /$$   /$$ /$$                                  /$$$$$$                                                  \n" +
                "| $$  /$$/| $$                                 /$$__  $$                                                 \n" +
                "| $$ /$$/ | $$$$$$$   /$$$$$$   /$$$$$$       | $$  \\__/ /$$   /$$  /$$$$$$  /$$   /$$  /$$$$$$$ /$$$$$$$\n" +
                "| $$$$$/  | $$__  $$ /$$__  $$ |____  $$      | $$      | $$  | $$ /$$__  $$| $$  | $$ /$$_____//$$_____/\n" +
                "| $$  $$  | $$  \\ $$| $$  \\ $$  /$$$$$$$      | $$      | $$  | $$| $$  \\__/| $$  | $$|  $$$$$$|  $$$$$$ \n" +
                "| $$\\  $$ | $$  | $$| $$  | $$ /$$__  $$      | $$    $$| $$  | $$| $$      | $$  | $$ \\____  $$\\____  $$\n" +
                "| $$ \\  $$| $$  | $$|  $$$$$$/|  $$$$$$$      |  $$$$$$/|  $$$$$$$| $$      |  $$$$$$/ /$$$$$$$//$$$$$$$/\n" +
                "|__/  \\__/|__/  |__/ \\______/  \\_______/       \\______/  \\____  $$|__/       \\______/ |_______/|_______/ \n" +
                "                                                         /$$  | $$                                       \n" +
                "                                                        |  $$$$$$/                                       \n" +
                "                                                         \\______/                                        ");

        generateData();
    }

    private void generateData() {
        LOGGER.info("[generateData]");
        LOGGER.info("Check Existing Email");
        if (!userAccountRepository.existsByEmail("khoacyruss@gmail.com")) {
            LOGGER.info("Create User Profile");
            UserProfileEntity userProfile = new UserProfileEntity();
            userProfile.setFirstName("Đinh Đăng");
            userProfile.setLastName("Khoa");
            userProfile.setGender(GenderFlag.MALE);
            userProfile.setStreet("496/63/2D Đường Dương Quảng Hàm");
            userProfile.setDistrict("Gò Vấp");
            userProfile.setCity("Hồ Chí Minh");
            userProfile.setProvide("Hồ Chí Minh");
            userProfile.setActiveFlag(ActiveFlag.ACTIVE);
            userProfile.setUsableFlag(UsableFlag.USABLE);
            userProfile.setCreateBy(MessageConstant.SYSTEM);
            userProfile.setCreateDate(LocalDateTime.now());
            userProfile.setUpdateBy(MessageConstant.SYSTEM);
            userProfile.setUpdateDate(LocalDateTime.now());
            userProfile = userProfileRepository.save(userProfile);

            LOGGER.info("Create User Account");
            UserAccountEntity userAccountEntity = new UserAccountEntity();
            userAccountEntity.setEmail("khoacyruss@gmail.com");
            userAccountEntity.setPassword(passwordEncoder.encode("123456"));
            userAccountEntity.setUserProfile(userProfile);
            userAccountEntity.setActiveFlag(ActiveFlag.ACTIVE);
            userAccountEntity.setUsableFlag(UsableFlag.USABLE);
            userAccountEntity.setCreateBy(MessageConstant.SYSTEM);
            userAccountEntity.setCreateDate(LocalDateTime.now());
            userAccountEntity.setUpdateBy(MessageConstant.SYSTEM);
            userAccountEntity.setUpdateDate(LocalDateTime.now());
            userAccountRepository.save(userAccountEntity);
        }
    }
}
