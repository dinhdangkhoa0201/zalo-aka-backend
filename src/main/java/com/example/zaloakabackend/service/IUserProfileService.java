package com.example.zaloakabackend.service;

import com.example.zaloakabackend.dto.RegisterDTO;
import com.example.zaloakabackend.dto.UserProfileDTO;
import com.example.zaloakabackend.model.ResponseResult;
import com.example.zaloakabackend.request.VerifyTokenRequest;

import java.util.List;

public interface IUserProfileService {
    ResponseResult<Object> register(RegisterDTO registerDTO);

    ResponseResult<Object> verify(VerifyTokenRequest request);

    ResponseResult<List<UserProfileDTO>> findAll();
}
