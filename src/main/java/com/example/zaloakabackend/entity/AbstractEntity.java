package com.example.zaloakabackend.entity;

import com.example.zaloakabackend.convertor.ActiveFlagConverter;
import com.example.zaloakabackend.convertor.UsableFlagConverter;
import com.example.zaloakabackend.enums.ActiveFlag;
import com.example.zaloakabackend.enums.UsableFlag;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Convert(converter = ActiveFlagConverter.class)
    private ActiveFlag activeFlag;

    @Convert(converter = UsableFlagConverter.class)
    private UsableFlag usableFlag;

    private String createBy;

    private LocalDateTime createDate;

    private String updateBy;

    private LocalDateTime updateDate;
}
