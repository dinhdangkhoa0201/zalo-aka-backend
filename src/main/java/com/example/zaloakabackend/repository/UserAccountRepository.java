package com.example.zaloakabackend.repository;

import com.example.zaloakabackend.entity.UserAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccountEntity, Long> {
    UserAccountEntity findByEmail(String email);

    Boolean existsByEmail(String  email);
}
