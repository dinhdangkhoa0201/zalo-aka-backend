package com.example.zaloakabackend.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "TBL_VALIDATION_TOKEN")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "activeFlag", column = @Column(name = "ACTIVE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "usableFlag", column = @Column(name = "USABLE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "createBy", column = @Column(name = "CREATE_BY")),
        @AttributeOverride(name = "createDate", column = @Column(name = "CREATE_DATE")),
        @AttributeOverride(name = "updateBy", column = @Column(name = "UPDATE_BY")),
        @AttributeOverride(name = "updateDate", column = @Column(name = "UPDATE_DATE")),
})
@Data
public class ValidationTokenEntity extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "EXPIRATION_DATE")
    private LocalDateTime expirationDate;

    @OneToOne
    @JoinColumn(name = "USER_ACCOUNT_ID")
    private UserAccountEntity userAccount;
}
