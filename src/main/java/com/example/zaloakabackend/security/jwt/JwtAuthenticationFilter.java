package com.example.zaloakabackend.security.jwt;

import com.example.zaloakabackend.constants.MessageConstant;
import com.example.zaloakabackend.security.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserService userService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        LOGGER.info("[doFilterInternal]");
        try {
            String jwt = getJwtFromRequest(request);

            if (StringUtils.isNoneBlank(jwt) && jwtTokenProvider.validateToken(jwt)) {
                Long userAccountId = jwtTokenProvider.getUserAccountIdFromJWT(jwt);
                UserDetails userDetails = userService.findById(userAccountId);
                if (Objects.nonNull(userDetails)) {
                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                            userDetails,
                            userDetails.getUsername(),
                            userDetails.getAuthorities());
                    authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        filterChain.doFilter(request, response);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        LOGGER.info("[getJwtFromRequest]");
        String token = request.getHeader("Authorization");
        if (StringUtils.isNotEmpty(token) && StringUtils.startsWith(token, MessageConstant.PREFIX_JWT)) {
            return token.substring(MessageConstant.PREFIX_JWT.length());
        }
        return null;
    }
}
