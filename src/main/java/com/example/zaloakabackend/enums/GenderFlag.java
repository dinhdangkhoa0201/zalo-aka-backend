package com.example.zaloakabackend.enums;

public enum GenderFlag {
    MALE("1", "Active"),
    FEMALE("0", "Inactive"),
    NULL("", "");
    private final String code;
    private final String desc;

    private GenderFlag(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static GenderFlag fromCode(String code) {
        if (code != null) {
            for (GenderFlag genderFlag : GenderFlag.values()) {
                if (genderFlag.getCode().equals(code)) {
                    return genderFlag;
                }
            }
        }
        return GenderFlag.NULL;
    }
}
