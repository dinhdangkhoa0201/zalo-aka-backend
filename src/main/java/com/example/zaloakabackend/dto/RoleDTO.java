package com.example.zaloakabackend.dto;

import lombok.Data;

@Data
public class RoleDTO extends AbstractDTO {

    private String code;

    private String name;

    private String desc;
}
