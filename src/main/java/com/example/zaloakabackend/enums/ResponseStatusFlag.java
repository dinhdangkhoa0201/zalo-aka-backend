package com.example.zaloakabackend.enums;

public enum ResponseStatusFlag {
    SUCCESS("1", "Success"),
    FAILURE("0", "Failure"),
    NULL("", "");
    private final String code;
    private final String desc;

    private ResponseStatusFlag(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ResponseStatusFlag fromCode(String code) {
        if (code != null) {
            for (ResponseStatusFlag responseStatusFlag : ResponseStatusFlag.values()) {
                if (responseStatusFlag.getCode().equals(code)) {
                    return responseStatusFlag;
                }
            }
        }
        return ResponseStatusFlag.NULL;
    }
}
