package com.example.zaloakabackend.service;

import com.example.zaloakabackend.dto.UserAccountDTO;
import com.example.zaloakabackend.model.ResponseResult;
import com.example.zaloakabackend.request.ChangePasswordRequest;

import java.util.List;

public interface IUserAccountService {
    ResponseResult<List<UserAccountDTO>> findAll();

    ResponseResult<UserAccountDTO> findById();

    ResponseResult<UserAccountDTO> findByEmail(String email);

    ResponseResult<Object> changePassword(Long id, ChangePasswordRequest request);
}
