package com.example.zaloakabackend.repository;

import com.example.zaloakabackend.entity.ValidationTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ValidationTokenRepository extends JpaRepository<ValidationTokenEntity, Long> {
    Optional<ValidationTokenEntity> findByToken(String token);

    void deleteByToken(String token);
}
