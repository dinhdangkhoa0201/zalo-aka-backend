package com.example.zaloakabackend.advise;

import com.example.zaloakabackend.response.ErrorMessage;
import com.example.zaloakabackend.service.impl.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@RestControllerAdvice
public class RestControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestControllerExceptionHandler.class);
    // 400
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.info("[handleMethodArgumentNotValid]");
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setStatusCode(HttpStatus.BAD_REQUEST);
        errorMessage.setTimestamp(LocalDateTime.now());
        errorMessage.setMessage(ex.getLocalizedMessage());
        for (FieldError error: ex.getBindingResult().getFieldErrors()) {
            errorMessage.getErrors().add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error: ex.getBindingResult().getGlobalErrors()) {
            errorMessage.getErrors().add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        errorMessage.setPath(request.getContextPath());
        return handleExceptionInternal(ex, errorMessage, headers, errorMessage.getStatusCode(), request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.info("[handleTypeMismatch]");
        String error = ex.getValue() + " value for " + ex.getPropertyName() + " should be of type " + ex.getRequiredType();
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setStatusCode(HttpStatus.BAD_REQUEST);
        errorMessage.setTimestamp(LocalDateTime.now());
        errorMessage.setMessage(ex.getLocalizedMessage());
        errorMessage.getErrors().add(error);
        errorMessage.setPath(request.getContextPath());
        return handleExceptionInternal(ex, errorMessage, headers, errorMessage.getStatusCode(), request);
    }

    // 404
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.info("[handleNoHandlerFoundException]");
        String error = "No handler found for " + ex.getHttpMethod() + " - " + ex.getRequestURL();
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setStatusCode(HttpStatus.NOT_FOUND);
        errorMessage.setTimestamp(LocalDateTime.now());
        errorMessage.setMessage(ex.getLocalizedMessage());
        errorMessage.getErrors().add(error);
        errorMessage.setPath(request.getContextPath());
        return handleExceptionInternal(ex, errorMessage, headers, errorMessage.getStatusCode(), request);
    }
}
