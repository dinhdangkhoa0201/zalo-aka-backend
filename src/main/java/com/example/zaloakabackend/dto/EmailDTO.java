package com.example.zaloakabackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailDTO implements Serializable {
    @NotNull
    @NotEmpty
    private List<EmailItemDTO> emailTo;

    private List<EmailItemDTO> emailCC;

    private List<EmailItemDTO> emailBCC;

    @NotNull
    @NotEmpty
    private String subject;

    @NotNull
    @NotEmpty
    private String content;
}
