package com.example.zaloakabackend.security.jwt;

import com.example.zaloakabackend.constants.MessageConstant;
import com.example.zaloakabackend.security.service.UserDetail;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenProvider.class);

    public String generateToken(UserDetail userDetail) {
        Date now = new Date();
        Date expireDate = new Date(now.getTime() + MessageConstant.JWT_EXPIRATION);

        return Jwts.builder()
                .setSubject(Long.toString(userDetail.getUserAccount().getId()))
                .setIssuedAt(now)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, MessageConstant.JWT_SECRET)
                .compact();
    }

    public Long getUserAccountIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(MessageConstant.JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();
        return Long.parseLong(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(MessageConstant.JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            LOGGER.error("[Invalid JWT token]");
        } catch (ExpiredJwtException ex) {
            LOGGER.error("[Expired JWT token]");
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("[Unsupported JWT token]");
        } catch (IllegalArgumentException ex) {
            LOGGER.error("[JWT claims string is empty]");
        }
        return false;
    }
}
