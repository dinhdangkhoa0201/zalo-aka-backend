package com.example.zaloakabackend.convertor;

import com.example.zaloakabackend.enums.GenderFlag;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class GenderFlagConverter implements AttributeConverter<GenderFlag, String> {
    @Override
    public String convertToDatabaseColumn(GenderFlag genderFlag) {
        if (genderFlag == null)
            return null;
        return genderFlag.getCode();
    }

    @Override
    public GenderFlag convertToEntityAttribute(String s) {
        if (s == null)
            return null;
        return GenderFlag.fromCode(s);
    }
}
