package com.example.zaloakabackend.service.impl;

import com.example.zaloakabackend.constants.ErrorMessageConstant;
import com.example.zaloakabackend.dto.UserAccountDTO;
import com.example.zaloakabackend.entity.UserAccountEntity;
import com.example.zaloakabackend.enums.ResponseStatusFlag;
import com.example.zaloakabackend.model.ResponseResult;
import com.example.zaloakabackend.repository.UserAccountRepository;
import com.example.zaloakabackend.request.ChangePasswordRequest;
import com.example.zaloakabackend.response.ErrorMessage;
import com.example.zaloakabackend.service.IUserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserAccountService implements IUserAccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAccountService.class);

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public ResponseResult<List<UserAccountDTO>> findAll() {
        LOGGER.info("[findAll]");
        return null;
    }

    @Override
    public ResponseResult<UserAccountDTO> findById() {
        return null;
    }

    @Override
    public ResponseResult<UserAccountDTO> findByEmail(String email) {
        LOGGER.info("[findByEmail]");
        return null;
    }

    @Override
    public ResponseResult<Object> changePassword(Long id, ChangePasswordRequest request) {
        LOGGER.info("[changePassword]");
        ResponseResult<Object> result = new ResponseResult<>(ResponseStatusFlag.FAILURE);
        try {
            Optional<UserAccountEntity> optional = userAccountRepository.findById(id);
            if (optional.isPresent()) {
                UserAccountEntity entity = optional.get();
                if (passwordEncoder.matches(request.getOldPassword(), entity.getPassword())) {
                    LOGGER.info(ErrorMessageConstant.PASSWORD_CORRECT);
                    entity.setPassword(passwordEncoder.encode(request.getNewPassword()));
                    entity.setUpdateBy(entity.getEmail());
                    entity.setUpdateDate(LocalDateTime.now());
                    entity = userAccountRepository.save(entity);
                    result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, entity);
                } else {
                    LOGGER.info(ErrorMessageConstant.PASSWORD_NOT_CORRECT);
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setStatusCode(HttpStatus.UNAUTHORIZED);
                    errorMessage.setTimestamp(LocalDateTime.now());
                    errorMessage.setMessage(ErrorMessageConstant.PASSWORD_NOT_CORRECT);
                    result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, errorMessage);
                }
            } else {
                LOGGER.info(ErrorMessageConstant.USER_ACCOUNT_NOT_FOUND);
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setStatusCode(HttpStatus.UNAUTHORIZED);
                errorMessage.setTimestamp(LocalDateTime.now());
                errorMessage.setMessage(ErrorMessageConstant.USER_ACCOUNT_NOT_FOUND);
                result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, errorMessage);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }
}
