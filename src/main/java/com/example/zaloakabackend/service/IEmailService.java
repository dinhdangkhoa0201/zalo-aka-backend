package com.example.zaloakabackend.service;

import com.example.zaloakabackend.dto.EmailDTO;

public interface IEmailService {
    void sendEmail(EmailDTO emailDTO);
}
