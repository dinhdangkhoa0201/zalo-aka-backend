package com.example.zaloakabackend.service.impl;

import com.example.zaloakabackend.constants.MessageConstant;
import com.example.zaloakabackend.dto.EmailDTO;
import com.example.zaloakabackend.dto.EmailItemDTO;
import com.example.zaloakabackend.service.IEmailService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.mail.internet.MimeMessage;

@Service
public class EmailService implements IEmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(EmailDTO emailDTO) {
        LOGGER.info("[sendEmail]");
        try {
            LOGGER.info("- To: " + emailDTO.getEmailTo());
            LOGGER.info("- CC: " + emailDTO.getEmailCC());
            LOGGER.info("- BCC: " + emailDTO.getEmailBCC());

            MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage);
            mimeMailMessage.setFrom(MessageConstant.EMAIL_USERNAME);
            if (!CollectionUtils.isEmpty(emailDTO.getEmailTo())) {
                mimeMessageHelper.setTo(emailDTO.getEmailTo().stream().map(EmailItemDTO::getEmail).toArray(String[]::new));
            }
            if (!CollectionUtils.isEmpty(emailDTO.getEmailCC())) {
                mimeMessageHelper.setCc(emailDTO.getEmailCC().stream().map(EmailItemDTO::getEmail).toArray(String[]::new));
            }
            if (!CollectionUtils.isEmpty(emailDTO.getEmailBCC())) {
                mimeMessageHelper.setBcc(emailDTO.getEmailBCC().stream().map(EmailItemDTO::getEmail).toArray(String[]::new));
            }
            mimeMessageHelper.setSubject(emailDTO.getSubject());
            mimeMailMessage.setContent("<strong>" + emailDTO.getContent() + "</strong>", "text/html");

            javaMailSender.send(mimeMailMessage);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
