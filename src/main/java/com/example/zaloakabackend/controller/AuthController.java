package com.example.zaloakabackend.controller;

import com.example.zaloakabackend.dto.ValidationTokenDTO;
import com.example.zaloakabackend.dto.RegisterDTO;
import com.example.zaloakabackend.dto.UserAccountDTO;
import com.example.zaloakabackend.dto.UserProfileDTO;
import com.example.zaloakabackend.entity.UserAccountEntity;
import com.example.zaloakabackend.enums.ResponseStatusFlag;
import com.example.zaloakabackend.model.ResponseResult;
import com.example.zaloakabackend.request.ChangePasswordRequest;
import com.example.zaloakabackend.request.LoginRequest;
import com.example.zaloakabackend.request.VerifyTokenRequest;
import com.example.zaloakabackend.response.ErrorMessage;
import com.example.zaloakabackend.response.LoginResponse;
import com.example.zaloakabackend.response.RegisterResponse;
import com.example.zaloakabackend.security.jwt.JwtTokenProvider;
import com.example.zaloakabackend.security.service.UserDetail;
import com.example.zaloakabackend.service.IUserAccountService;
import com.example.zaloakabackend.service.IUserProfileService;
import com.example.zaloakabackend.service.IValidationTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/auth")
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private IValidationTokenService refreshTokenService;

    @Autowired
    private IUserProfileService iUserProfileService;

    @Autowired
    private IUserAccountService iUserAccountService;

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequest request) {
        LOGGER.info("[login]");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetail userDetail = (UserDetail) authentication.getPrincipal();
        String jwt = jwtTokenProvider.generateToken(userDetail);

        // Get User Account
        UserAccountEntity userAccountEntity = userDetail.getUserAccount();
        UserAccountDTO userAccountDTO = new UserAccountDTO();
        BeanUtils.copyProperties(userAccountEntity, userAccountDTO);

        // Get User Profile
        UserProfileDTO userProfileDTO = new UserProfileDTO();
        BeanUtils.copyProperties(userAccountEntity.getUserProfile(), userProfileDTO);
        userAccountDTO.setUserProfile(userProfileDTO);

        // Initial Login Response
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setUserAccount(userAccountDTO);
        loginResponse.setToken(jwt);
        // Generate Refresh Token
        ResponseResult<ValidationTokenDTO> result = refreshTokenService.createRefreshToken(userAccountDTO.getId());
        if (result.getStatus() == ResponseStatusFlag.SUCCESS && result.getObject() != null) {
            loginResponse.setRefreshToken(result.getObject().getToken());
        }
        return new ResponseEntity<>(loginResponse, HttpStatus.OK);
    }

    @RequestMapping(path = "/authenticate", method = RequestMethod.GET)
    public ResponseEntity<?> authenticate(HttpServletRequest request) {
        LOGGER.info("[authenticate]");
        final Map<String, Object> response = new HashMap<>();
        response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        response.put("status", HttpServletResponse.SC_OK);
        response.put("message", "Authenticate successfully");
        response.put("path", request.getServletPath());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody RegisterDTO registerDTO) {
        ResponseResult<Object> result = iUserProfileService.register(registerDTO);
        if (result.getObject() instanceof RegisterResponse) {
            RegisterResponse registerResponse = (RegisterResponse) result.getObject();
            return new ResponseEntity<>(registerResponse, HttpStatus.OK);
        }
        ErrorMessage errorMessage = (ErrorMessage) result.getObject();
        return new ResponseEntity<>(errorMessage, errorMessage.getStatusCode());
    }

    @RequestMapping(path = "/verify", method = RequestMethod.POST)
    public ResponseEntity<?> verify(@RequestBody VerifyTokenRequest request) {
        LOGGER.info("[verify]");
        return new ResponseEntity<>(iUserProfileService.verify(request), HttpStatus.OK);
    }

    @RequestMapping(path = "/changePassword/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@PathVariable("id") Long id, @RequestBody ChangePasswordRequest request) {
        LOGGER.info("[changePassword]");
        return new ResponseEntity<>(iUserAccountService.changePassword(id, request).getObject(), HttpStatus.OK);
    }
}
