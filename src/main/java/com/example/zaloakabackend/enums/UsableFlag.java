package com.example.zaloakabackend.enums;

public enum UsableFlag {
    USABLE("1", "Usable"),
    UNUSABLE("0", "Unusable"),
    NULL("", "");
    private final String code;
    private final String desc;

    private UsableFlag(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static UsableFlag fromCode(String code) {
        if (code != null) {
            for (UsableFlag usableFlag : UsableFlag.values()) {
                if (usableFlag.getCode().equals(code)) {
                    return usableFlag;
                }
            }
        }
        return UsableFlag.NULL;
    }
}
