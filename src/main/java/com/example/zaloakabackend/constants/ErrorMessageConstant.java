package com.example.zaloakabackend.constants;

public class ErrorMessageConstant {
    // EMAIL
    public static final String EMAIL_EXISTED = "Email is existed!";

    public static final String EMAIL_INVALID_FORMAT = "Email is invalid format!";

    // PASSWORD
    public static final String PASSWORD_NOT_CORRECT = "Password is NOT correct!";

    public static final String PASSWORD_CORRECT = "Password is correct!";

    // USER ACCOUNT
    public static final String USER_ACCOUNT_NOT_FOUND = "User Account is not found!";

    // TOKEN
    public static final String TOKEN_EXPIRED = "Token is expired!";
}
