package com.example.zaloakabackend.service;

import com.example.zaloakabackend.dto.SystemConstantDTO;
import com.example.zaloakabackend.model.ResponseResult;

import java.util.List;

public interface ISystemConstantService {
    ResponseResult<List<SystemConstantDTO>> findAll();
}
