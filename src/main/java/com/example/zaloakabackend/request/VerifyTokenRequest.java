package com.example.zaloakabackend.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class VerifyTokenRequest implements Serializable {
    private String token;
}
