package com.example.zaloakabackend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1")
public class IndexController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping(path = "/health", method = RequestMethod.GET)
    public ResponseEntity<?> checkHealth() {
        LOGGER.info("[checkHealth]");
        return ResponseEntity.status(200).body(null);
    }
}
