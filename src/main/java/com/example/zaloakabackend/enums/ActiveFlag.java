package com.example.zaloakabackend.enums;

public enum ActiveFlag {
    ACTIVE("1", "Active"),
    INACTIVE("0", "Inactive"),
    NULL(null, "");
    private final String code;
    private final String desc;

    private ActiveFlag(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ActiveFlag fromCode(String code) {
        if (code != null) {
            for (ActiveFlag activeFlag : ActiveFlag.values()) {
                if (activeFlag.getCode().equals(code)) {
                    return activeFlag;
                }
            }
        }
        return ActiveFlag.NULL;
    }
}
