package com.example.zaloakabackend.repository;

import com.example.zaloakabackend.entity.SystemConstantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemConstantRepository extends JpaRepository<SystemConstantEntity, Long> {
}
