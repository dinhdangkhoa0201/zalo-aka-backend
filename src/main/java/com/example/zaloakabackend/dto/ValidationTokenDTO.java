package com.example.zaloakabackend.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Data
public class ValidationTokenDTO extends AbstractDTO implements Serializable {
    @NotNull
    @NotEmpty
    private String token;

    @NotNull
    private Instant expirationDate;

    @NotNull
    private UserAccountDTO userAccount;
}
