package com.example.zaloakabackend.service;

import com.example.zaloakabackend.dto.RoleDTO;
import com.example.zaloakabackend.model.ResponseResult;

import java.util.List;

public interface IRoleService {
    ResponseResult<List<RoleDTO>> findAll();
}
