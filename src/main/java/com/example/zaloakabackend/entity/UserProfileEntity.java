package com.example.zaloakabackend.entity;

import com.example.zaloakabackend.convertor.GenderFlagConverter;
import com.example.zaloakabackend.enums.GenderFlag;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TBL_M_USER_PROFILE")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "activeFlag", column = @Column(name = "ACTIVE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "usableFlag", column = @Column(name = "USABLE_FLAG", columnDefinition = "CHAR(1)")),
        @AttributeOverride(name = "createBy", column = @Column(name = "CREATE_BY")),
        @AttributeOverride(name = "createDate", column = @Column(name = "CREATE_DATE")),
        @AttributeOverride(name = "updateBy", column = @Column(name = "UPDATE_BY")),
        @AttributeOverride(name = "updateDate", column = @Column(name = "UPDATE_DATE")),
})
@Data
public class UserProfileEntity extends AbstractEntity implements Serializable {

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "GENDER", columnDefinition = "CHAR(1)")
    @Convert(converter = GenderFlagConverter.class)
    private GenderFlag gender;

    @Column(name = "STREET")
    private String street;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "CITY")
    private String city;

    @Column(name = "PROVINCE")
    private String provide;
}
