package com.example.zaloakabackend.dto;

import lombok.Data;

@Data
public class SystemConstantDTO extends AbstractDTO {
    private String code;

    private String message;

    private String desc;
}
