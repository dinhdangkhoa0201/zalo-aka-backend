package com.example.zaloakabackend.dto;

import com.example.zaloakabackend.enums.GenderFlag;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UserProfileDTO extends AbstractDTO {

    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastName;

    private GenderFlag gender;

    private String street;

    private String district;

    private String city;

    private String provide;
}
