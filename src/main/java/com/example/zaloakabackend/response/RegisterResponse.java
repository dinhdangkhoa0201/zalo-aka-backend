package com.example.zaloakabackend.response;

import com.example.zaloakabackend.dto.UserAccountDTO;
import com.example.zaloakabackend.dto.UserProfileDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterResponse implements Serializable {
    private UserAccountDTO userAccountDTO;

    private String token;
}
