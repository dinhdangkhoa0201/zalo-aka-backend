package com.example.zaloakabackend.service.impl;

import com.example.zaloakabackend.constants.MessageConstant;
import com.example.zaloakabackend.dto.ValidationTokenDTO;
import com.example.zaloakabackend.entity.ValidationTokenEntity;
import com.example.zaloakabackend.entity.UserAccountEntity;
import com.example.zaloakabackend.enums.ActiveFlag;
import com.example.zaloakabackend.enums.ResponseStatusFlag;
import com.example.zaloakabackend.enums.UsableFlag;
import com.example.zaloakabackend.model.ResponseResult;
import com.example.zaloakabackend.repository.ValidationTokenRepository;
import com.example.zaloakabackend.repository.UserAccountRepository;
import com.example.zaloakabackend.service.IValidationTokenService;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class ValidationTokenService implements IValidationTokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationTokenService.class);

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private ValidationTokenRepository validationTokenRepository;

    @Override
    public ResponseResult<ValidationTokenDTO> createRefreshToken(Long userAccountId) {
        LOGGER.info("[createRefreshToken]");
        ResponseResult<ValidationTokenDTO> result = new ResponseResult<>(ResponseStatusFlag.FAILURE);
        try {
            Optional<UserAccountEntity> optional = userAccountRepository.findById(userAccountId);
            if (optional.isPresent()) {
                ValidationTokenEntity entity = new ValidationTokenEntity();
                entity.setToken(RandomStringUtils.randomAlphabetic(10));
                entity.setExpirationDate(LocalDateTime.now().plusSeconds(MessageConstant.VALIDATION_EXPIRED_TIME));
                entity.setUserAccount(optional.get());
                entity.setActiveFlag(ActiveFlag.ACTIVE);
                entity.setUsableFlag(UsableFlag.USABLE);
                entity.setCreateBy(MessageConstant.SYSTEM);
                entity.setCreateDate(LocalDateTime.now());
                entity.setUpdateBy(MessageConstant.SYSTEM);
                entity.setUpdateDate(LocalDateTime.now());
                entity = validationTokenRepository.save(entity);

                ValidationTokenDTO dto = ValidationTokenDTO.class.newInstance();
                BeanUtils.copyProperties(entity, dto);
                result = new ResponseResult<>(ResponseStatusFlag.SUCCESS, dto);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            result.setMessage(e.getMessage());
        }
        return result;
    }
}
