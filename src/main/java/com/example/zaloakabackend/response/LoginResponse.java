package com.example.zaloakabackend.response;

import com.example.zaloakabackend.constants.MessageConstant;
import com.example.zaloakabackend.dto.RoleDTO;
import com.example.zaloakabackend.dto.UserAccountDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {
    private UserAccountDTO userAccount;
    private String refreshToken;
    private String token;
    private String prefixToken = MessageConstant.PREFIX_JWT;

    public LoginResponse(String token) {
        this.token = token;
    }
}
