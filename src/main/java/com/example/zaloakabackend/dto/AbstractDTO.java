package com.example.zaloakabackend.dto;


import com.example.zaloakabackend.enums.ActiveFlag;
import com.example.zaloakabackend.enums.UsableFlag;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public abstract class AbstractDTO {

    private Long id;

    @NotNull
    private ActiveFlag activeFlag;

    @NotNull
    private UsableFlag usableFlag;

    @NotNull
    @NotEmpty
    private String createBy;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime createDate;

    @NotNull
    @NotEmpty
    private String updateBy;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime updateDate;
}
