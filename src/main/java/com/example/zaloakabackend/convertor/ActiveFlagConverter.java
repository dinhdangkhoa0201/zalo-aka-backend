package com.example.zaloakabackend.convertor;

import com.example.zaloakabackend.enums.ActiveFlag;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ActiveFlagConverter implements AttributeConverter<ActiveFlag, String> {
    @Override
    public String convertToDatabaseColumn(ActiveFlag activeFlag) {
        if (activeFlag == null)
            return null;
        return activeFlag.getCode();
    }

    @Override
    public ActiveFlag convertToEntityAttribute(String s) {
        if (s == null)
            return ActiveFlag.NULL;
        return ActiveFlag.fromCode(s);
    }
}
