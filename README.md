# Zalo Aka

> Zalo Aka Message App


## Features

### 1. Features:

#### 1.1 Authencation:
 - [x] Login
 - [x] Register
 - [x] Change Password
 - [x] Verify Account
 - [ ] Forgot Password
 - [x] Send Email
 
#### 1.2 User Account:
 - [ ] Dashboard (View)
 - [ ] Filter/Search
 - [ ] User Account Profile (View)

#### 1.3 Friend List:
 - [ ] Search By Email
 - [ ] Search By Phone
 - [ ] View Info
 - [ ] Send Add Friend Request
 - [ ] Accept/Reject Add Friend Request

#### 1.4 User Profile:

 - [ ] View Profile
 - [ ] Change Info
 - [ ] Upload 

### 2. Tools:

#### 2.1 Back end
- **Language**:
	- Java 8
- **Framework**:
	- Spring Boot Framework
- **Library**
	- Spring Boot
	- Spring Security
	- Spring Mail
	- Spring Validation
	- JWT
	- Redis
	- Swagger 2

#### 2.2 Front end
-	**Language**:
	-	JavaScript
	-	Typescript
- **Framework**
	-  ReactJS

#### 2.3 Tools
 - IntelliJ
 - MySQL
 - Postman
 - HeidiSQL

	 
